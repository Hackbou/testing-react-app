import React, { useState } from 'react';
import './section.scss';
import { Button, Container } from '@mui/material';
import { Col, Row } from 'react-bootstrap';
import { Add, EastTwoTone, More, ShoppingBagOutlined } from '@mui/icons-material';

function Section() {

  const [category, setCategory] = useState('home');

  const home = () => {
    setCategory('home');
  }

  const newspaper = () => {
    setCategory('newspaper');
  }

  const chevronLeftSharp = () => {
    setCategory('chevronLeftSharp');
  }

  const help = () => {
    setCategory('help');
  }

  return (
    <>
      <div className='sect1'>
        <Container className="section-1-blok text-center py-5">
          <h3 className="h1">Je suis qu'un titre</h3>
          <p className="paragraph">
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repellat voluptatem, officiis repudiandae voluptas illum fugit veniam sit, ducimus culpa possimus nisi sed cumque perferendis rem doloribus vitae, eveniet consequatur labore.
          </p>
          <p className="paragraph">
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repellat voluptatem, officiis repudiandae voluptas illum fugit veniam sit, ducimus culpa possimus nisi sed cumque perferendis rem doloribus vitae, eveniet consequatur labore.
          </p>
          <Button>
            <More className="more-icon" /> Connaitre tout nos service
          </Button>
        </Container>
      </div>

      <div className="section-blog sect2">
        {
          category === 'home' && (
            <img className="section-blog-img" src="assets/products/product_16.jpg" alt="" />
          )
        }

        {
          category === 'newspaper' && (
            <img className="section-blog-img" src="assets/products/product_23.jpg" alt="" />
          )
        }

        {
          category === 'chevronLeftSharp' && (
            <img className="section-blog-img" src="assets/products/product_13.jpg" alt="" />
          )
        }

        {
          category === 'help' && (
            <img className="section-blog-img" src="assets/products/product_10.jpg" alt="" />
          )
        }
        <Container className="py-5">
          <Row>
            {
              category === 'home' && (
                <Col className="col-md-6 col-sm-12 col-xs-12 mr-3 blok-left">
                  <h3>Accessoty</h3>
                  <p>Lorem ipsum dolor sit amet illum luptatum et in autem. Nostrud amet illum accusam tempor accusam magna amet lorem vel sanctus ipsum praesent ea duis sed diam. Consequat dolor iriure dolor stet voluptua ipsum at gubergren et sea nostrud aliquyam. Autem est sea et dolores facilisis. Vero erat at vel no.</p>
                  <Button className="sec2-btn">
                    <Add /> Voire Plus
                  </Button>
                </Col>
              )
            }

            {
              category === 'newspaper' && (
                <Col className="col-md-6 col-sm-12 col-xs-12 mr-3 blok-left">
                  <h3>newspaper</h3>
                  <p>Lorem ipsum dolor sit amet illum luptatum et in autem. Nostrud amet illum accusam tempor accusam magna amet lorem vel sanctus ipsum praesent ea duis sed diam. Consequat dolor iriure dolor stet voluptua ipsum at gubergren et sea nostrud aliquyam. Autem est sea et dolores facilisis. Vero erat at vel no.</p>
                  <Button className="sec2-btn">
                    <Add /> Voire Plus
                  </Button>
                </Col>
              )
            }

            {
              category === 'chevronLeftSharp' && (
                <Col className="col-md-6 col-sm-12 col-xs-12 mr-3 blok-left">
                  <h3>chevronLeftSharp</h3>
                  <p>Lorem ipsum dolor sit amet illum luptatum et in autem. Nostrud amet illum accusam tempor accusam magna amet lorem vel sanctus ipsum praesent ea duis sed diam. Consequat dolor iriure dolor stet voluptua ipsum at gubergren et sea nostrud aliquyam. Autem est sea et dolores facilisis. Vero erat at vel no.</p>
                  <Button className="sec2-btn">
                    <Add /> Voire Plus
                  </Button>
                </Col>
              )
            }

            {
              category === 'help' && (
                <Col className="col-md-6 col-sm-12 col-xs-12 mr-3 blok-left">
                  <h3>help</h3>
                  <p>Lorem ipsum dolor sit amet illum luptatum et in autem. Nostrud amet illum accusam tempor accusam magna amet lorem vel sanctus ipsum praesent ea duis sed diam. Consequat dolor iriure dolor stet voluptua ipsum at gubergren et sea nostrud aliquyam. Autem est sea et dolores facilisis. Vero erat at vel no.</p>
                  <Button className="sec2-btn">
                    <Add /> Voire Plus
                  </Button>
                </Col>
              )
            }
            <Col className="col-md-6 col-12 blok-right">
              <div className="blok-right-img">
                {
                  category === 'home' && (
                    <img src="assets/products/product_16.jpg" alt="" />
                  )
                }

                {
                  category === 'newspaper' && (
                    <img src="assets/products/product_23.jpg" alt="" />
                  )
                }

                {
                  category === 'chevronLeftSharp' && (
                    <img src="assets/products/product_13.jpg" alt="" />
                  )
                }

                {
                  category === 'help' && (
                    <img src="assets/products/product_10.jpg" alt="" />
                  )
                }
              </div>
              <div className="blok-right-categories">
                <ul>
                  <li onClick={home} > <img src="assets/products/product_16.jpg" alt="" /> </li>
                  <li onClick={newspaper}> <img src="assets/products/product_23.jpg" alt="" /> </li>
                  <li onClick={chevronLeftSharp}> <img src="assets/products/product_13.jpg" alt="" /> </li>
                  <li onClick={help}> <img src="assets/products/product_10.jpg" alt="" /> </li>
                </ul>
              </div>
            </Col>
          </Row>
        </Container>
      </div >

      <div className="container mb-5 pb-5 sect3">
        <Row>
          <Col className="col-md-6 col-12 mb-4">
            <div className="block-sect2">
              <div className="block-sect2-icons">
                <ShoppingBagOutlined className="block-sect2-icon" />
              </div>
              <h3>Acheter en toute securite</h3>
              <p>
                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Labore velit nobis sapiente dolorum! Minima necessitatibus nostrum nemo voluptatum ullam, maxime deleniti! Iusto, quas libero quae repudiandae vel minus ipsum harum.
              </p>
            </div>
          </Col>
          <Col className="col-md-6 col-12">
            <div className="block-sect2">
              <div className="block-sect2-icons">
                <EastTwoTone className="block-sect2-icon" />
              </div>
              <h3>Foncer et decouvrer nos produits</h3>
              <p>
                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Labore velit nobis sapiente dolorum! Minima necessitatibus nostrum nemo voluptatum ullam, maxime deleniti! Iusto, quas libero quae repudiandae vel minus ipsum harum.
              </p>
            </div>
          </Col>
        </Row>
      </div>
    </>
  )
}

export default Section