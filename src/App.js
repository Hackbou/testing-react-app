import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import './App.scss';
import SingleProduct from './pages/singleProduct/SingleProduct';
import Footer from './components/footer/Footer';
import Home from './pages/home/Home';

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="singleProduct" element={<SingleProduct />} />
      </Routes>

      <Footer />
    </Router>
  );
}

export default App;
