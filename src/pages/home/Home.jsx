import React from 'react'
import Favorites from '../../components/favorite/Favorites'
import Header from '../../components/header/Header'
import Section from '../../components/section/Section'

function Home() {
  return (
    <div>
      <Header />
        <Section />
        <Favorites />
    </div>
  )
}

export default Home